package tdd.training.bsk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class FrameTest {	

	@Test
	// test user story 1
	public void test() {
		Frame frame = new Frame(2, 4);
		assertEquals(2, frame.getFirstThrow());
		assertEquals(4, frame.getSecondThrow());
	}

	@Test
	// test user story 2
	public void testGetFrameScore() throws BowlingException{		
		Frame frame = new Frame(2, 6);
		assertEquals(8, frame.getScore());
	}	

	@Rule
	public ExpectedException exception = ExpectedException.none();
	@Test
	// test user story 2 with exception
	public void testGetFrameScoreException() throws BowlingException {
		exception.expect(BowlingException.class);
		exception.expectMessage("Lo score del frame è invalido (score > 10)");

		Frame frame = new Frame(2, 9);
		frame.getScore();
	}

	@Test
	// test user story 3
	public void testGetGame() throws BowlingException{
		Game game = new Game();
		Frame frame = new Frame(2, 6);
		game.addFrame(frame);
		int index=0;

		assertEquals(frame, game.getFrameAt(index));
	}

	@Rule
	public ExpectedException exception2 = ExpectedException.none();
	@Test
	// test user story 3
	public void testGetGameException() throws BowlingException{
		exception.expect(BowlingException.class);
		exception.expectMessage("Cannot add more than 10 frames per game.");

		Game game = new Game();
		game.addFrame(new Frame(2,6));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		game.addFrame(new Frame(2,6));
	}


	@Test
	// test user story 4
	public void testCalculateGameScore() throws BowlingException{
		Game game = new Game();
		int score = 81;

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		assertEquals(score, game.calculateScore());
	}


	@Test
	// test user story 5 isSpare()
	public void testGetGameScoreIsSpare() throws BowlingException{
		Frame f = (new Frame(1,9));
		assertTrue(f.isSpare());
	}

	@Test
	// test user story 5 spare
	public void testGetGameScoreWithSpare() throws BowlingException{
		int score = 88;
		Game game = new Game();

		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		assertEquals(score, game.calculateScore());
	}

	@Test
	// test user story 6 isStrike()
	public void testGetGameScoreIsStrike() {
		Frame f = (new Frame(10,0));
		assertTrue(f.isStrike());
	}


	@Test
	// test user story 6 strike
	public void testGetGameScoreWithStrike() throws BowlingException{
		int score = 94;
		Game game = new Game();

		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		assertEquals(score, game.calculateScore());
	}

	@Test
	// test user story 7 strike and spare
	public void testGetGameScoreWithStrikeAndSpare() throws BowlingException {
		int score = 103;
		Game game = new Game();

		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		assertEquals(score, game.calculateScore());
	}

	@Test
	// test user story 8 multiple strike
	public void testGetGameScoreWithMultipleStrike() throws BowlingException {
		int score = 112;
		Game game = new Game();

		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		assertEquals(score, game.calculateScore());
	}

	@Test
	// test user story 9 multiple spare
	public void testGetGameScoreWithMultipleSpare() throws BowlingException {
		int score = 98;
		Game game = new Game();

		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		assertEquals(score, game.calculateScore());
	}

	@Test
	// test user story 10
	public void testGetFirstBonusThrow() throws BowlingException {
		int bonusThrow = 7;
		Game game = new Game();

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(bonusThrow);

		assertEquals(bonusThrow, game.getFirstBonusThrow());
	}

	@Test
	// test user story 10 calculate score when last frame is spare
	public void testCalculateScoreWithLastFrameSpare() throws BowlingException {
		int score = 90;
		Game game = new Game();

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		
		game.setFirstBonusThrow(7);

		assertEquals(score, game.calculateScore());
	}
	
	@Test
	// test user story 11
	public void testGetSecondBonusThrow() throws BowlingException {
		Game game = new Game();

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);

		assertEquals(2, game.getSecondBonusThrow());
	}
	
	@Test
	// test user story 11 calculate score when last frame is strike
	public void testCalculateScoreWithLastFrameStrike() throws BowlingException {
		int score = 92;
		Game game = new Game();

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(score, game.calculateScore());
	}
	
	@Test
	// test user story 12 calculate score when last frame is strike
	public void testPerfectGame() throws BowlingException{
		int score = 300;
		Game game = new Game();

		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(score, game.calculateScore());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
