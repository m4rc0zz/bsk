package tdd.training.bsk;

public class Game {
	protected Frame[] frames;
	protected int index;
	protected int firstBonusThrow;
	protected int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.frames = new Frame[10];
		this.index = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {

		if(this.index<10) {
			this.frames[this.index] = frame;
			index++;
		} else {
			throw new BowlingException("Cannot add more than 10 frames per game.");
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {
		return this.frames[index];
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for(int i=0; i<frames.length; i++) {
			//spare finale
			if(frames[i].isSpare() && i==9) {
				score = score + frames[i].getScore() + this.firstBonusThrow;
			}
			// spare in un frame che non è l'ultimo
			else if(frames[i].isSpare()) {
				frames[i].setBonus(frames[i+1].getFirstThrow());
				score = score + frames[i].getScore();
			}
			else if(frames[i].isStrike() && i==8) {
				frames[i].setBonus(frames[i+1].getScore() + this.firstBonusThrow);
				score = score + frames[i].getScore();
			}
			//strike finale
			else if(frames[i].isStrike() && i==9) {
				score = score + frames[i].getScore() + this.firstBonusThrow + this.secondBonusThrow;
			}
			else if(frames[i].isStrike()) {
				// due strike consecutivi
				if(frames[i+1].isStrike()) {
					frames[i].setBonus(frames[i+1].getScore() + frames[i+2].getFirstThrow());
					score = score + frames[i].getScore();
				}
				//un solo strike
				else {
					frames[i].setBonus(frames[i+1].getScore());
					score = score + frames[i].getScore();
				}
			}
			else score = score + frames[i].getScore();
		}
		return score;
	}

}
